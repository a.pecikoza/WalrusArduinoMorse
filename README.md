# Project Title

Projet Walrus Arduino

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

 - Node.js
 - npm (or Yarn)
 - Docker
 - Docker-compose
 - Arduino IDE

### Installing

Copy the project 

run `npm install` in both project

run Arduino IDE and load the code into your Arduino


## How to run
```
sudo docker-compose up //into the server
sudo node Walrus.js //into the client
```
Connect into port 3000 on your web browser


## Authors

 - Thibault BRENTOT
 - Antoine DUVAL
 - Michel NGUYEN
 - Alexandre PECIKOZA
 - Romain PORET

## License
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details