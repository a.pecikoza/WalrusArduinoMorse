#define SWITCH_PIN 2

int is_pressed = 0;
int buttonState = 0;
int incomingByte;

void setup() {
    // put your setup code here, to run once:
    Serial.begin(9600);

    pinMode(SWITCH_PIN, INPUT);
}

void ispressed(){
    Serial.println(1);
}

void isreleased(){
    Serial.println(2);
}

void loop() {
    // put your main code here, to run repeatedly:
    buttonState = digitalRead(SWITCH_PIN);
    if(buttonState == HIGH){
        if(is_pressed == 0){
            is_pressed = 1;
            ispressed();
        }
    }else{
        if(is_pressed == 1){
            is_pressed = 0;
            isreleased();
        }
    }
}