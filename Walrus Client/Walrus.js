//Define requires
const express = require("express");
const serialport = require("serialport");
const socket = require("socket.io");
const http = require("http");
const socket_client_io = require("socket.io-client");

//Store Arduino data
var myData = 0;
var mac_code = 123456789;
//Setup and start web server on :3000
const app = express();
var server = http.createServer(app);
var io = socket(server);
var socket_client = socket_client_io.connect("http://172.16.22.42:8080" );

socket_client.on("connect", function() {
  console.log("envoyé");
  socket_client.emit("new_arduino", JSON.stringify([""+mac_code+""]));
});

socket_client.on("connect_error", function(){
  console.log("Connection failed");
});

app.use(express.static(__dirname + "/public/"));
app.get("/", function(req, res) {
  res.sendFile(__dirname + "/public/index.html");
});
server.listen(80, function() {
  console.log("Site sur le port 80!");
});

//Setup and start serial port reading
const Readline = serialport.parsers.Readline;
const parser = new Readline();
var mySerialPort = new serialport("/dev/ttyACM0", {
  baudRate: 9600,
  dataBits: 8,
  parity: "none",
  stopBits: 1,
  flowControl: false
});
mySerialPort.pipe(parser);
parser.on("data", function(input) {
  console.log("un clique", input);
  if(input==1){
    socket_client.emit("morse_press", JSON.stringify([]));
  }
  else if(input==2){
    socket_client.emit("morse_release", JSON.stringify([]));
  }
});
