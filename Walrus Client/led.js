var five = require("johnny-five"),
    board = new five.Board();

board.on("ready", function() {
  // Create an Led on pin 13
  var led_pin_send = new five.Led(9);
  var led_pin_rec = new five.Led(11);
  var switch_pin = new five.Button(2)

  // Strobe the pin on/off, defaults to 100ms phases
  led_pin_rec.strobe();
});
