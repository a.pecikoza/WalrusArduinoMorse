module.exports = {
	init : function(connection){
		// ON REMET A ZERO LA TABLE DES CONNEXIONS
		this.connection_reset(connection, function(){

		});
	},

	/*
		####################################################################################################################################################
		##### MESSAGE ######################################################################################################################################
		####################################################################################################################################################
	*/

	// CETTE FONCTION PERMET DE RECUPERER LES CONVERSATIONS D'UN UTILISATEUR
	insert_message : function(connection, message, conversation_id, user_id, callback){
		d = new Date();
		date = d.getFullYear()+"-"+(d.getMonth()+1)+"-"+d.getDate()+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();
		connection.query("INSERT INTO `message` (`ID`, `user_id`, `text`, `date`, `conversation_id`) VALUES (NULL, ?, ?, ?, ?);", [user_id, message, date, conversation_id], function (error, results){
			callback(error, results, conversation_id, user_id, d.getDate()+" "+(d.getMonth()+1)+" "+d.getFullYear()+" "+d.getHours()+":"+d.getMinutes());
		});
	},

	/*
		####################################################################################################################################################
		##### CONVERSATION #################################################################################################################################
		####################################################################################################################################################
	*/

	// CETTE FONCTION PERMET DE RECUPERER LA CONVERSATION DONT FONT PARTI EXCLUSIVEMENT CES UTILISATEURS
	select_conversations_by_all_users : function(connection, users_id, callback){
		add_one = "";
		add_two = "";
		for(cpt = 0; cpt<users_id.length; cpt++){
			if(cpt > 0){
				add_one += " AND ";
				add_two += ", ";
			}
			add_one += " `conversation_id` IN (SELECT `conversation_id` FROM message WHERE user_id = "+users_id[cpt]+") ";
			add_two += users_id[cpt];
		}
		connection.query("SELECT DISTINCT `conversation_id` FROM `message` WHERE "+add_one+" AND `conversation_id` NOT IN (SELECT `conversation_id` FROM message WHERE user_id NOT IN ("+add_two+"));", [], function (error, results){
			callback(error, results);
		});
	},

	// CETTE FONCTION PERMET DE RECUPERER LES CONVERSATIONS D'UN UTILISATEUR
	select_conversations_by_user_id : function(connection, user_id, callback){
		connection.query("SELECT DISTINCT c.ID, c.title FROM conversation c JOIN message m ON m.conversation_id = c.id WHERE m.user_id = ?;", [user_id], function (error, results){
			callback(error, results);
		});
	},

	// CETTE FONCTION PERMET DE RECUPERER LA LISTE DES ID QUI FONT PARTIE D'UNE CONVERSATION
	select_conversation_list_users_id : function(connection, conversation_id, user_id, date, callback){
		connection.query("SELECT DISTINCT user_id FROM message WHERE conversation_id = ?;", [conversation_id], function (error, results){
			callback(error, results, conversation_id, user_id, date);
		});
	},

	// CETTE FONCTION PERMET DE RECUPERER LE DERNIER MESSAGE ET SA DATE D'UNE CONVERSATION
	select_conversation_last_date_and_message : function(connection, conversation_id, conversation_title, cpt, max, callback){
		connection.query("SELECT m.date, m.text FROM conversation c JOIN message m ON m.conversation_id = c.id WHERE c.ID = ? ORDER BY date DESC LIMIT 1;", [conversation_id], function (error, results){
			callback(error, results, conversation_id, conversation_title, cpt, max);
		});
	},

	// CETTE FONCTION PERMET DE RECUPERER LE DERNIER MESSAGE ET SA DATE D'UNE CONVERSATION
	select_conversation_list_users : function(connection, user_id, conversation_id, conversation_title, conversation_text, conversation_date, cpt, max, callback){
		connection.query("SELECT GROUP_CONCAT(DISTINCT email SEPARATOR ', ') as title FROM user u JOIN message m ON m.user_id = u.ID WHERE m.conversation_id = ? AND m.user_id != ?;", [conversation_id, user_id], function (error, results){
			callback(error, results, conversation_id, conversation_title, conversation_text, conversation_date, cpt, max);
		});
	},

	// CETTE FONCTION RETOURNE TOUT LES MESSAGES D'UNE CONVERSATION PAR CONVERSATION_ID SI L'UTILISATEUR EST DEDANS
	select_messages_by_conversation_id : function(connection, conversation_id, user_id, callback){
		connection.query("SELECT m.*, u.email FROM message m JOIN conversation c ON c.ID = m.conversation_id JOIN user u ON u.ID = m.user_id WHERE text != '' AND c.ID = ? AND EXISTS (select user_id from message where conversation_id = ? AND user_id = ?) ORDER BY date;", [conversation_id, conversation_id, user_id], function (error, results){
			callback(error, results);
		});
	},

	/*
		####################################################################################################################################################
		##### USER #########################################################################################################################################
		####################################################################################################################################################
	*/

	// CETTE FONCTION PERMET DE RECUPERER UN UTILISATEUR PAR SON ID
	select_user_by_id : function(connection, user_id, callback){
		connection.query("SELECT * FROM `user` WHERE `ID` = ?;", [user_id], function (error, results){
			callback(error, results);
		});
	},

	// CETTE FONCTION PERMET DE RECUPERER UN UTILISATEUR PAR SON EMAIL
	select_user_like_email : function(connection, email, callback){
		connection.query("SELECT * FROM `user` WHERE `email` LIKE ? ORDER BY email ASC;", [email+"%"], function (error, results){
			callback(error, results);
		});
	},

	// CETTE FONCTION PERMET DE RECUPERER UN UTILISATEUR PAR SON EMAIL ET SON MOT DE PASSE
	select_user_by_email_and_password : function(connection, email, password, callback){
		connection.query("SELECT * FROM user WHERE email = ? AND password = ?;", [email, password], function (error, results){
			callback(error, results);
		});
	},

	// CETTE FONCTION PERMET DE CREER UN UTILISATEUR
	insert_user : function(connection, email, password, callback){
		connection.query("INSERT INTO `user` (`ID`, `email`, `email_checked`, `password`) VALUES (NULL, ?, '0', ?);", [email, password], function (error, results){
			callback(error, results);
		});
	},

	// CETTE FONCTION RETOURNE L'UTILISATEUR PAR SON CODE SECRET
	user_select_by_secret : function(connection, secret, callback){
		connection.query('SELECT u.* FROM user u JOIN connection c ON c.user_id = u.id WHERE c.secret = ?', [secret], function (error, results, fields){
			if(error)
				throw error;
			else{
				if(results[0] != undefined)
					callback(results[0]);
				else
					callback(false);
			}
		});
	},

	/*
		####################################################################################################################################################
		##### CONNECTION ###################################################################################################################################
		####################################################################################################################################################
	*/

		// CETTE FONCTION VIDE LA TABLE DES CONNEXIONS
		connection_reset : function(connection, callback){
			connection.query("TRUNCATE TABLE connection", [], function (error, results, fields){
				if(error)
					throw error;
				else{
					callback();
				}
			});
		},

		// CETTE FONCTION CREE UNE CONNEXION
	  connection_insert : function(connection, user_id, secret, callback){
			connection.query('INSERT INTO connection SET ?', {user_id: user_id, secret: secret}, function (error, results, fields){
				if(error)
					throw error;
				else{
					callback();
				}
			});
	  },
};
