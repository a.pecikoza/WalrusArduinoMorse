var sequence = "";

var morseCode = (function() {

	this._dotTime = 250;
	this._dashTime = 750;
	this._pauseTime = 250;

	this._code  = [
					new Array('A', '+-'  ),
					new Array('B', '-+++'),
					new Array('C', '-+-+'),
					new Array('D', '-++' ),
					new Array('E', '+'   ),
					new Array('F', '++-+'),
					new Array('G', '--+' ),
					new Array('H', '++++'),
					new Array('I', '++'  ),
					new Array('J', '+---'),
					new Array('K', '-+-' ),
					new Array('L', '+-++'),
					new Array('M', '--'  ),
					new Array('N', '-+'  ),
					new Array('O', '---' ),
					new Array('P', '+--+'),
					new Array('Q', '--+-'),
					new Array('R', '+-+' ),
					new Array('S', '+++' ),
					new Array('T', '-'   ),
					new Array('U', '++-' ),
					new Array('V', '+++-'),
					new Array('W', '+--' ),
					new Array('X', '-++-'  ),
					new Array('Y', '-+--'),
					new Array('Z', '--++')
				  ];


	this.getDashTime = function(){
		return ( this._dashTime );
	};

	this.getDotTime = function(){
		return ( this._dotTime );
	};

	this.getPauseTime = function(){
		return ( this._pauseTime );
	};

	this.getCode = function(){
		return ( this._code );
	};
});


var timeoutWord = (function(){
	var elem = $("body .box#home .right .bottom textarea");
	elem.val(elem.val()+" ");
});

var timeoutSend = (function(){
	var e = jQuery.Event("keydown");
	e.which = 13;
	$("body .box#home .right .bottom textarea").trigger(e);
});

var timeoutLetter = (function(){
	solver()
});

var solver = (function(){
	var regex = sequence.replace(",", "");
	let morse = new morseCode;
	var correspondance = morse.getCode();

	for(var i=0; i < correspondance.length; i++){
		//alert("r: " + sequence + " / c: " + correspondance[i][1]);
		if(sequence == correspondance[i][1]){
			var elem = $("body .box#home .right .bottom textarea");
			elem.val(elem.val()+correspondance[i][0]);
		}
		else{

		}
	}

	sequence = "";
});
