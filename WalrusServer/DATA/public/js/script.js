$(function(){
  var server = "172.16.22.42";
  var port = 8080;
  var socket = io.connect(server+':'+port);
  var dev = true;




	let morse = new morseCode;
	var dotTime = morse.getDotTime();
	var pauseTime = morse.getPauseTime();
	var keyDownTime1 = null;
	var letterTimer = null;
	var wordTimer = sendTimer = null;

  /* ########################################################################################################################################################## */
  /* ### EVENT ################################################################################################################################################ */
  /* ########################################################################################################################################################## */

  // SI LE SERVEUR DONNE UN ORDRE AU CLIENT, LE CLIENT L'EXECUTE.
  socket.on('execute', function(data){
    if(dev == true)
      console.log(data);
    eval(data);
  });

  // SI L'UTILISATEUR ENFONCE SA TOUCHE DE MORSE
  socket.on('morse_press', function(data){
    if(keyDownTime1)
      return;
		clearTimeout(wordTimer);
		clearTimeout(letterTimer);
    clearTimeout(sendTimer);
		keyDownTime1 = new Date();
  });

  // S'IL Y A UN SOUCI DE CONNECTION AU SERVEUR
  socket.on('connect_error', function(){
    $('body .box#login .center .logo').addClass('angry');
    $('body .box#login .bottom').removeClass('connected');
    $('body .box#login .center .form').removeClass('connected');
    $('body .box#login .center h1').removeClass('connected');
    $('body .box#login .bottom .v_center').removeClass('connected');
    $('body .box#login .center .form').removeClass('show');
  });

  // SI L'UTILISATEUR RELACHE SA TOUCHE DE MORSE
  socket.on('morse_release', function(data){
		var pressureTime = (new Date() - keyDownTime1);
		keyDownTime1 = null;
    if(pressureTime <= dotTime){
      sequence += "+";
    }
    else{
      sequence += "-";
    }

    letterTimer = setTimeout(timeoutLetter, pauseTime * 3);
    wordTimer = setTimeout(timeoutWord, 3500);
    sendTimer = setTimeout(timeoutSend, 10000);
  });

  $("body ").on("mousedown", ".box#home .right .bottom div", function(){
    socket.emit("link_arduino", JSON.stringify([
			""+123456789
		]));
  });

  // SI L'UTILISATEUR TENTE DE S'INSCRIRE
  $("body").on("click", ".box#login .center .form .tab:nth-child(2) button", function(){
    socket.emit("subscribe_try", JSON.stringify([
			$("body .box#login .center .form .tab:nth-child(2) input[type='email']").val(),
			$("body .box#login .center .form .tab:nth-child(2) input[type='password']:nth-child(2)").val(),
			$("body .box#login .center .form .tab:nth-child(2) input[type='password']:nth-child(3)").val()
		]));
  });

  // SI L'UTILISATEUR TENTE DE SE CONNECTER
  $("body").on("click", ".box#login .center .form .tab:nth-child(1) button", function(){
    socket.emit("login_try", JSON.stringify([
			$("body .box#login .center .form .tab:nth-child(1) input[type='email']").val(),
			$("body .box#login .center .form .tab:nth-child(1) input[type='password']").val()
		]));
  });

  // SI L'UTILISATEUR CLIQUE SUR LE LOGO DU SITE
  $("body").on("click", ".box#login .center .logo", function(){
    new_tab  = $("body .box#login .center .form .tab:not(.active)");
    $("body .box#login .center .form .tab").removeClass("active");
    new_tab.addClass("active");
  });

  // SI L'UTILISATEUR CLIQUE SUR LE LOGO DU SITE
  $("body").on("keyup", ".box#home .left .bottom input[type='email']", function(){
    socket.emit("search_email", JSON.stringify([
			$("body .box#home .left .bottom input[type='email']").val()
		]));
  });

  // SI L'UTILISATEUR CLIQUE SUR UNE CONVERSATION
  $("body").on("click", ".box#home .left .bottom ul.conversations li", function(){
    $("body .box#home .right .top").html($(this).find(".title").html());
    $("body .box#home .right").attr("conv_num", $(this).attr("conv_num"));
    $("body .box#home .right").attr("user_num", 0);
    socket.emit("get_messages", JSON.stringify([
			$("body .box#home .right").attr("conv_num")
		]));
  });

  // SI L'UTILISATEUR CLIQUE SUR L'EMAIL D'UN AMI DANS LA RECHERCHE
  $("body").on("click", ".box#home .left .bottom ul.search li", function(){
    existant = $("body .box#home .left .bottom ul.conversations li[conv_num='"+$(this).attr('conv_num')+"']");
    // SI L'ON A DEJA UNE CONVERSATION AVEC CETTE PERSONNE
    if(existant.length){
      existant.click();
    }
    else{
      $("body .box#home .right .top").html($(this).html().split("</i>")[1]);
      $("body .box#home .right").attr("conv_num", 0);
      $("body .box#home .right").attr("user_num", $(this).attr("user_num"));
      $("body .box#home .right .middle").html("");
    }
    // ON VIDE LE CHAMP DE RECHERCHE
    $("body .box#home .left .bottom ul.search").removeClass("active");
    $("body .box#home .left .bottom input[type='email']").val("");
  });

  // SI L'UTILISATEUR ECRIS DANS LA BARRE DE CHAT
  $("body").on("keyup", ".box#home .right .bottom textarea", function(){
    if($(this).val() == ""){
      $("body .box#home .right .bottom button").removeClass("active");
    }
    else{
      $("body .box#home .right .bottom button").addClass("active");
    }
  });

  // SI L'UTILISATEUR ENVOIE LE MESSAGE AVEC LA TOUCHE ENTREE
  $("body").on("keydown", ".box#home .right .bottom textarea", function(e){
    if(e.which == 13){
      $("body .box#home .right .bottom button").click();
      e.preventDefault();
    }
  });

    // SI L'UTILISATEUR ENVOIE LE MESSAGE AVEC LA BOUTON
  $("body").on("click", ".box#home .right .bottom button", function(){
    socket.emit("new_message", JSON.stringify([
			$("body .box#home .right .bottom textarea").val(),
			$("body .box#home .right").attr("user_num"),
			$("body .box#home .right").attr("conv_num")
		]));
  });
});
