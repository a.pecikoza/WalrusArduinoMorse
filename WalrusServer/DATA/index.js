var bdd        = require('./bdd');
var functions  = require('./functions');

var fs         = require('fs');      // https://www.npmjs.com/package/fs
var http       = require('http');
var express    = require('express'); // https://www.npmjs.com/package/express
var request    = require('request'); // https://www.npmjs.com/package/request
var app        = express();

var server       = http.createServer(app);
var port         = 8080;
var io           = require('socket.io')(server, {
});
var sha1         = require('sha1');   // https://www.npmjs.com/package/sha1
var ent          = require('ent');    // https://www.npmjs.com/package/ent
var mysql        = require('mysql');  // https://www.npmjs.com/package/mysql
var colors       = require('colors'); // https://www.npmjs.com/package/colors
var connection = mysql.createConnection({
	host     : 'db',
	user     : 'walrus',
	password : 'walrus',
	database : 'walrus'
});
connection.connect();
bdd.init(connection);

var data = [];
data["users"] = [];
data["arduinos"] = [];

app.use(express.static(__dirname + '/public/'));
app.get("/", function(req, res){
	res.sendFile(__dirname+"/public/index.html");
});

/* ############################################################################################################################### */
/* ### SERVER #################################################################################################################### */
/* ############################################################################################################################### */

io.on('connection', function(client){
	console.log("un mec essaie de se co");
	// QUAND L'UTILISATEUR SE PRESENTE, ON AFFICHE L'INTERFACE DE CONNECTION.

	client.emit("execute", "$('body .box#login .center .logo').removeClass('angry');");
	client.emit("execute", "$('body .box#login .bottom').addClass('connected');");
  client.emit("execute", "$('body .box#login .center .form').addClass('connected');");
  client.emit("execute", "$('body .box#login .center h1').addClass('connected');");
	client.emit("execute", "setTimeout(function(){$('body .box#login .bottom .v_center').addClass('connected');}, 2000);");
	client.emit("execute", "setTimeout(function(){$('body .box#login .center .form').addClass('show');}, 2000);");

	// SI L'UTILISATEUR ESSAIE DE SE CONNECTER
	client.on('login_try', function(json){
		functions.server_login_try(json, ent, sha1, data, client, bdd, connection);
	});

	// SI L'UTILISATEUR ESSAIE DE S'INSCRIRE
	client.on('subscribe_try', function(json){
		functions.server_subscribe_try(json, ent, sha1, data, client, bdd, connection);
	});

	// SI L'UTILISATEUR RECHERCHE UNE EMAIL
	client.on('search_email', function(json){
		functions.server_search_email(json, ent, sha1, data, client, bdd, connection);
	});

	// SI L'UTILISATEUR ENVOIE UN NOUVEAU MESSAGE
	client.on('new_message', function(json){
		functions.server_new_message(json, ent, sha1, data, client, bdd, connection);
	});

	// SI L'UTILISATEUR DEMANDE A RECUPERER LE CONTENU D'UNE CONVERSATION
	client.on('get_messages', function(json){
		functions.server_get_messages(json, ent, sha1, data, client, bdd, connection);
	});

	// SI L'UTILISATEUR ENFONCE SA TOUCHE DE MORSE
	client.on('morse_press', function(json){
		functions.server_morse_touch("press", ent, sha1, data, client, bdd, connection);
	});

	// SI L'UTILISATEUR RELACHE SA TOUCHE DE MORSE
	client.on('morse_release', function(json){
		functions.server_morse_touch("release", ent, sha1, data, client, bdd, connection);
	});

	// SI L'UTILISATEUR RELACHE SA TOUCHE DE MORSE
	client.on('new_arduino', function(json){
		functions.server_new_arduino(json, ent, sha1, data, client, bdd, connection);
	});

	// SI L'UTILISATEUR RELACHE SA TOUCHE DE MORSE
	client.on('link_arduino', function(json){
		functions.server_link_arduino(json, ent, sha1, data, client, bdd, connection);
	});

	// QUAND UN UTILISATEUR SE DECONNECTE AU SERVEUR NODE
  client.on('disconnect', function(){
		functions.server_disconnect(data, client);
	});
});

server.listen(port);
