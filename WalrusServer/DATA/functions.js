module.exports = {
  /* ######################################################################################################################################################################### */
  /* ### FONCTIONS ########################################################################################################################################################### */
  /* ######################################################################################################################################################################### */

  // CETTE FONCTION PERMET DE TESTER SI LA CHAINE DE CARACTERE PASSEE EN PARAMETRE EST UNE EMAIL OU NON
  is_email : function(email){
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
	},



  // CETTE FONCTION RETOURNE UN MOT ALEATOIRE DE LA TAILLE DESIRE
	random_password : function(size){
  	var dictionnary = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
  	var word = "";
  	for(var cpt = 0; cpt<size; cpt++){
  		word += dictionnary[Math.floor(Math.random() * dictionnary.length-1)];
  	}
  	return word;
	},



  // CETTE FONCTION PERMET DE SAVOIR SI UN UTILISATEUR EST DEJA CONNECTE OU NON
	is_connected : function(data, id){
  	for(i = 0; i < data["users"].length; i++){
  		if(data["users"][i].client.conn.id == id){
  			return data["users"][i].user_id;
  		}
  	}
  	return 0;
	},



  // CETTE FONCTION RETOURNE UN MESSAGE FORMATE A AFFICHER
  message_content : function(side, text, date){
  	return "<div class='message "+side+"'><div class='circle'></div><div class='text'><div class='date'>"+date+"</div>"+text+"</div></div>";
	},



  // CETTE FONCTION RETOURNE LE MOIS EN LETTRES
  number_to_month : function(number){
    array = ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"];
  	return array[number];
	},

  /* ######################################################################################################################################################################### */
  /* ### SERVER ############################################################################################################################################################## */
  /* ######################################################################################################################################################################### */

  // CETTE FONCTION PERMET A L'UTILISATEUR DE SE CONNECTER
  server_login_try : function(json, ent, sha1, data, client, bdd, connection){
    array = JSON.parse(json);
		email = ent.encode(array[0]);
		password = ent.encode(array[1]);
		if(email == "" && password == "")
      client.emit("execute", "$('body .box#login .center .logo .message').html(\"Vous ne pouvez vous identifier si vous ne fournissez ni adresse e-mail ni mot de passe !\").addClass('active');");
		else if(email == "")
      client.emit("execute", "$('body .box#login .center .logo .message').html(\"Vous ne pouvez vous identifier si vous ne fournissez pas d'adresse e-mail !\").addClass('active');");
		else if(password == "")
      client.emit("execute", "$('body .box#login .center .logo .message').html(\"Vous ne pouvez vous identifier si vous ne fournissez pas de mot de passe !\").addClass('active');");
		else{
			if(this.is_email(email)){
				if(this.is_connected(data, client.conn.id) == 0){
        	crypted_password = sha1(password);
          self = this;
          bdd.select_user_by_email_and_password(connection, email, crypted_password, function(err, result){
            if(err)
              throw err;
            else if(result[0] != undefined){
              var secret = self.random_password(100);
              bdd.connection_insert(connection, result[0].ID, secret, function(){
                user = [];
                user.client = client;
                user.user_id = result[0].ID;
                user.secret = secret;
                data["users"].push(user);

                // ON RECHERCHE SA LISTE DE CONVERSATION
                bdd.select_conversations_by_user_id(connection, user.user_id, function(err, conversations){
                  if(err)
                    throw err;
                  else{
                    for(cpt=0; cpt<conversations.length; cpt++){
                      bdd.select_conversation_last_date_and_message(connection, conversations[cpt].ID, conversations[cpt].title, cpt, conversations.length-1, function(err, messages, conversation_id, conversation_title, cpt, max){
                        if(err)
                          throw err;
                        else{
                          bdd.select_conversation_list_users(connection, user.user_id, conversation_id, conversation_title, messages[0].text, messages[0].date, cpt, max, function(err, results, conversation_id, conversation_title, conversation_text, conversation_date, cpt, max){
                            title = conversation_title;
                            if(conversation_title == "")
                              title = results[0].title;

                            txt  = "<li conv_num='"+conversation_id+"'>";
                            txt += "  <i class='fa fa-user-circle' aria-hidden='true'></i>";
                            txt += "  <p class='title'>"+title+"</p>";
                            txt += "  <p class='message'>"+conversation_text+"</p>";
                            txt += "</li>";
                            client.emit("execute", "$('body .box#home .left .bottom ul.conversations').append(\""+txt+"\");");
                            if(cpt == max)
                              client.emit("execute", "$('body .box#home .left .bottom ul.conversations li').click();");
                          });
                        }
                      });
                    }
                  }
                  client.emit("execute", "$('body .box#login .center .logo .message').html(\"\").removeClass('active');");
                  client.emit("execute", "$('body .box#login .bottom .v_center').removeClass('connected');");
                  client.emit("execute", "$('body .box#login').addClass('home');");
                  client.emit("execute", "$('body .box#login .center .form').removeClass('show');");
                });
              });
            }
            else
              client.emit("execute", "$('body .box#login .center .logo .message').html(\"Les identifiants ou les mots de passe entré sont incorrect !\").addClass('active');");
          });
				}
				else
					client.emit("execute", "$('body .box#login .center .logo .message').html(\"Vous êtes déjà connecté !\").addClass('active');");
			}
			else
				client.emit("execute", "$('body .box#login .center .logo .message').html(\"Votre adresse e-mail est invalide !\").addClass('active');");
		}
  },

  // CETTE FONCTION PERMET A L'UTILISATEUR DE S'INSCRIRE
  server_subscribe_try : function(json, ent, sha1, data, client, bdd, connection){
    array = JSON.parse(json);
		email = ent.encode(array[0]);
		password1 = ent.encode(array[1]);
		password2 = ent.encode(array[2]);
		if(email == "" && (password1 == "" || password2 == ""))
      client.emit("execute", "$('body .box#login .center .logo .message').html(\"Vous ne pouvez vous inscrire si vous ne fournissez ni adresse e-mail ni mot de passe !\").addClass('active');");
		else if(email == "")
      client.emit("execute", "$('body .box#login .center .logo .message').html(\"Vous ne pouvez vous inscrire si vous ne fournissez pas d'adresse e-mail !\").addClass('active');");
		else if(password1 == "" || password2 == "")
      client.emit("execute", "$('body .box#login .center .logo .message').html(\"Vous ne pouvez vous inscrire si vous ne fournissez pas de mot de passe !\").addClass('active');");
		else{
			if(this.is_email(email)){
        if(password1 == password2){
  				if(this.is_connected(data, client.conn.id) == 0){
          	crypted_password = sha1(password1);
            self = this;
            bdd.insert_user(connection, email, crypted_password, function(err, results){
              if(err)
                throw err;
              else{
      					client.emit("execute", "$('body .box#login .center .logo .message').html(\"L'inscription s'est bien passé, je vous invite à vous connecter.\").addClass('active');");
        				client.emit("execute", "$('body .box#login .center .tab:first-child input[type=\"email\"]').val('"+email+"');");
        				client.emit("execute", "$('body .box#login .center .tab:first-child input[type=\"password\"]').val('');");
        				client.emit("execute", "$('body .box#login .center .logo').click();");
        				client.emit("execute", "$('body .box#login .center .tab:last-child input').val('');");
              }
            });
  				}
  				else
  					client.emit("execute", "$('body .box#login .center .logo .message').html(\"Vous ne pouvez créer de compte si vous êtes connecté !\").addClass('active');");
        }
        else
          client.emit("execute", "$('body .box#login .center .logo .message').html(\"Vos mots de passe ne correspondent pas !\").addClass('active');");
			}
			else
				client.emit("execute", "$('body .box#login .center .logo .message').html(\"Votre adresse e-mail est invalide !\").addClass('active');");
		}
  },

  // SI L'UTILISATEUR RECHERCHE UNE EMAIL
  server_search_email : function(json, ent, sha1, data, client, bdd, connection){
    array = JSON.parse(json);
		email = ent.encode(array[0]);
		if(email != ""){
			if(this.is_connected(data, client.conn.id) != 0){
        self = this;
        bdd.select_user_like_email(connection, email, function(err, results){
          if(err)
            throw err;
          if(results[0] != undefined){
            txt = "";
            for(cpt = 0; cpt < results.length; cpt++){
              conv_num = 0;
              txt += "<li conv_num='"+conv_num+"' user_num='"+results[cpt].ID+"'>";
              txt += "<i class='fa fa-user-circle' aria-hidden='true'></i>"+results[cpt].email;
              txt += "</li>";
            }
      			client.emit("execute", "$('body .box#home .left .bottom ul.search').html('').addClass('active').append(\""+txt+"\");");
          }
          else{
    				client.emit("execute", "$('body .box#home .left .bottom ul.search').html('').addClass('active').append(\"<li>Aucun mail ne correspond à votre recherche...</li>\");");
          }
        });
			}
    }
    else
      client.emit("execute", "$('body .box#home .left .bottom ul.search').removeClass('active');");
  },

  // SI L'UTILISATEUR DEMANDE A RECEVOIR LE CONTENU D'UNE CONVERSATION
  server_get_messages : function(json, ent, sha1, data, client, bdd, connection){
    array = JSON.parse(json);
		conversation_id = ent.encode(array[0]);
    user_id = this.is_connected(data, client.conn.id);
		if(user_id != 0){
      self = this;
      bdd.select_messages_by_conversation_id(connection, conversation_id, user_id, function(err, results){
        client.emit("execute", "$('body .box#home .right .middle').html('');");
        for(cpt = 0; cpt<results.length; cpt++){
          side = "left";
          if(results[cpt].user_id == user_id)
            side = "right";
          client.emit("execute", "$('body .box#home .right .middle').append(\""+self.message_content(side, results[cpt].text, results[cpt].date)+"\");");
        }
        client.emit("execute", "$('body .box#home .right .middle').scrollTop(999999999999999999);");
      });
    }
  },

  // SI L'UTILISATEUR ENVOIE UN NOUVEAU MESSAGE
  server_new_message : function(json, ent, sha1, data, client, bdd, connection){
    array = JSON.parse(json);
		text = ent.encode(array[0]);
		destinataire_id = ent.encode(array[1]).split(",");
		conversation_id = ent.encode(array[2]);
    if(text != ""){
      user_id = this.is_connected(data, client.conn.id);
			if(user_id != 0){
        self = this;
        bdd.insert_message(connection, text, conversation_id, user_id, function(err, results, conversation_id, user_id, date){
          client.emit("execute", "$('body .box#home .right .bottom textarea').val('');");
          bdd.select_conversation_list_users_id(connection, conversation_id, user_id, date, function(err, results, conversation_id, user_id, date){
            for(cpt = 0; cpt<results.length; cpt++){
              for(cpt2 = 0; cpt2 < data["users"].length; cpt2++){
            		if(data["users"][cpt2].user_id == results[cpt].user_id){
                  side = "left";
                  if(data["users"][cpt2].user_id == user_id)
                    side = "right";
                  data["users"][cpt2].client.emit("execute", "$('body .box#home .right[conv_num=\""+conversation_id+"\"] .middle').append(\""+self.message_content(side, text, date)+"\");");
                  data["users"][cpt2].client.emit("execute", "$('body .box#home .right .middle').scrollTop(999999999999999999);");
            		}
            	}
            }
          });
        });
      }
    }
  },

  // SI UN ARDUINO ESSAIE DE JOINDRE LE SERVEUR
  server_new_arduino : function(json, ent, sha1, data, client, bdd, connection){
    array = JSON.parse(json);
		arduino_code = ent.encode(array[0]);
    arduino = [];
    arduino.client = client;
    arduino.code = arduino_code;
    arduino.user_id = 0;
    data["arduinos"].push(arduino);
  },

  server_link_arduino : function(json, ent, sha1, data, client, bdd, connection){
    array = JSON.parse(json);
		arduino_id = ent.encode(array[0]);
    user_id = this.is_connected(data, client.conn.id);
    if(user_id != 0){
      for(cpt = 0; cpt<data["arduinos"].length; cpt++){
        if(data["arduinos"][cpt].code == arduino_id){
          if(data["arduinos"][cpt].user_id == 0){
            data["arduinos"][cpt].user_id = user_id;
          }
        }
      }
    }
  },

  // SI L'UTILISATEUR ENFONCE SA TOUCHE DE MORSE
  server_morse_touch : function(message, ent, sha1, data, client, bdd, connection){
    for(a = 0; a<data["arduinos"].length; a++){
  		if(data["arduinos"][a].client.conn.id == client.conn.id){
        for(b = 0; b<data["users"].length; b++){
          if(data["arduinos"][a].user_id == data["users"][b].user_id){
            data["users"][b].client.emit("morse_"+message, "");
          }
        }
      }
    }
  },

  server_disconnect : function(data, client){
    for(i = 0; i < data["users"].length; i++){
  		if(data["users"][i].client.conn.id == client.conn.id){
        data["users"].splice(i, 1);
  		}
  	}
    for(i = 0; i < data["arduinos"].length; i++){
  		if(data["arduinos"][i].client.conn.id == client.conn.id){
        data["arduinos"].splice(i, 1);
  		}
  	}
  },
};
